##car_forum
本爬虫爬去 易车网的信息。

## 数据库使用, 请查看
[https://github.com/scalingexcellence/scrapybook/tree/master/ch09/properties](https://github.com/scalingexcellence/scrapybook/tree/master/ch09/properties)

配置数据库

在setting.py 中, 找到 MYSQL_PIPELINE_URL 参数填入自己的数据库

```
MYSQL_PIPELINE_URL='mysql://root:123456@127.0.0.1:3306/cars'
```


###创建表格

```
CREATE TABLE `bitauto` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `brand` varchar(255) DEFAULT NULL,
  `car_type` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `sub_type` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `question` text,
  `question_img` varchar(20) DEFAULT NULL,
  `replay` text,
  `replay_img` varchar(20) DEFAULT NULL,
  `replay_to` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `url` (`url`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
```


##运行爬虫

### 安装必要的工具

本爬虫是基于scrapy 框架进行书写使用mysql 进行数据存储, 所以需要安装scrapy 和 mysql-connector-python

```
$ pip install scrapy
$ pip install mysql-connector-python
```

## 如何使用

切换当前路径进入car_forum ,然后运行下面这个命令产生 csv

```
$ scrapy crawl bitauto -o out.csv
```

生成mqsql
 
```
$ scrapy crawl bitauto -o 
```
