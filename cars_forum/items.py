# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html
# 提取字断

from scrapy import Item, Field

class CarsForumItem(Item):
    # define the fields for your item here like:
    # name = Field()
    topic_id = Field()
    brand = Field() # 商标
    car_type = Field() # 车子类型
    type = Field() # 一级分类
    sub_type = Field() # 二级分类

    title = Field() # 标题
    question = Field() # 问题
    question_img = Field() # 问题图片

    replay = Field() # 回答
    replay_img = Field() # 回答图片
    replay_to = Field() # 回复给

    url = Field() # 路由地址
    datetime = Field() # 爬数据的时间